# DXVK

Camada de compatibilidade para o Direct3D 9/10/11 sob Vulkan, que permite executar aplicações gráficas feitas originalmente para Microsoft Windows no Linux.

Esta versão é baseada no [projeto original](https://github.com/doitsujin/dxvk), mas mantendo sempre as últimas atualizações e vem com o [async patch](https://github.com/Sporif/dxvk-async) aplicado por padrão.

Este projeto foi criado para ser usado no *Proton-Async*, que é uma remasterização do [Proton.](https://github.com/ValveSoftware/Proton)

# Compilando e usando

Maiores informações podem ser encontradas no git do [projeto original.](https://github.com/doitsujin/dxvk)
